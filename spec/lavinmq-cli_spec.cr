require "./spec_helper"

class Hello < Command
  getter slug : String = "hello"
  getter description : String = "Show hello"
  getter args : String = "to the world"
  getter specific_options : String = ""

  def validate?
    true
  end

  def process_command
    @io.puts @parser
  end
end

class World < Command
  getter slug : String = "world"
  getter description : String = "Show world"
  getter args : String = "<no_args>"
  getter specific_options : String = ""

  def validate?
    true
  end

  def process_command
    @io.puts "World"
  end
end

describe Cli do
  it "list available command with description" do
    io = IO::Memory.new
    cli = Cli.new([Hello.new, World.new], io)
    ARGV.push("-h")
    cli.run
    output = io.rewind.gets_to_end
    output.should contain "Usage: "
    output.should contain "hello"
    output.should contain "Show hello\n"
    output.should contain "world"
  end

  it "show hello run command" do
    io = IO::Memory.new
    cli = Cli.new([Hello.new(io), World.new(io)], io)
    ARGV.push("hello")
    ARGV.push("--help")
    cli.run
    output = io.rewind.gets_to_end
    output.should contain "Usage : #{PROGRAM_NAME} hello [OPTIONS] to the world"
  end
end

describe Command do
  it "show global options in help" do
    io = IO::Memory.new
    command = Hello.new(io)
    ARGV.push("--help")
    command.run
    output = io.rewind.gets_to_end
    output.should contain "-p vhost"
    output.should contain "-H host"
    output.should contain "-n node"
    output.should contain "-q"
    output.should contain "-s"
  end
end

describe AddUserCommand do
  it "add an user" do
    io = IO::Memory.new
    command = AddUserCommand.new(io)
    ARGV.push("-u")
    ARGV.push("user_test_add_command")
    ARGV.push("-w")
    ARGV.push("plop")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "User Created\n"
  end

  it "return help if username missing" do
    io = IO::Memory.new
    command = AddUserCommand.new(io)
    "-w plop".split(" ").each { |value| ARGV.push value }
    command.run
    output = io.rewind.gets_to_end
    output.should contain "Usage : #{PROGRAM_NAME} add_user"
  end
end

describe ChangePasswordCommand do
  it "change password for a existing user" do
    io = IO::Memory.new
    command = ChangePasswordCommand.new(io)
    ARGV.push("-u")
    ARGV.push("user_test_add_command")
    ARGV.push("-w")
    ARGV.push("pikachu")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "User Modified\n"
  end
end

describe ListUserCommand do
  it "show the list of users" do
    io = IO::Memory.new
    command = ListUserCommand.new(io)
    command.run
    output = io.rewind.gets_to_end
    output.should contain "Listing users ..."
    output.should contain "guest"
  end

  it "not show header when is quiet" do
    io = IO::Memory.new
    command = ListUserCommand.new(io)
    ARGV.push("-q")
    command.run
    output = io.rewind.gets_to_end
    output.should_not contain "Listing users ..."
  end
end

describe SetUserTagsCommand do
  it "change tags for a user" do
    io = IO::Memory.new
    command = SetUserTagsCommand.new(io)
    ARGV.push("-u")
    ARGV.push("user_test_add_command")
    ARGV.push("-t")
    ARGV.push("administrator,monitoring")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "User tags Modified\n"
  end

  it "return a error message if tags doesn't exist" do
    io = IO::Memory.new
    command = SetUserTagsCommand.new(io)
    ARGV.push("-u")
    ARGV.push("user_test_add_command")
    ARGV.push("-t")
    ARGV.push("pikacu")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Wrong tags\n"
  end
end

describe DeleteUserCommand do
  it "delete user" do
    io = IO::Memory.new
    command = DeleteUserCommand.new(io)
    ARGV.push("-u")
    ARGV.push("user_test_add_command")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "User deleted\n"
  end
end

describe StatusCommand do
  it "return many informations about server instance" do
    io = IO::Memory.new
    command = StatusCommand.new(io)
    command.run
    output = io.rewind.gets_to_end
    output.should contain "Version: "
    output.should contain "Node: "
    output.should contain "Uptime: "
    output.should contain "Connections: "
    output.should contain "Channels: "
    output.should contain "Consumers: "
    output.should contain "Exchanges: "
    output.should contain "Queues: "
    output.should contain "Messages: "
    output.should contain "Messages ready: "
    output.should contain "Messages unacked: "
  end
end

describe AddVhostCommand do
  it "create a vhost" do
    io = IO::Memory.new
    command = AddVhostCommand.new(io)
    ARGV.push("-p")
    ARGV.push("add_vhost_test")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Vhost added\n"
  end
end

describe ListVhostCommand do
  it "list all vhost" do
    io = IO::Memory.new
    command = ListVhostCommand.new(io)
    command.run
    output = io.rewind.gets_to_end
    output.should contain "Listing vhosts ..."
    output.should contain "name"
    output.should contain "/"
    output.should contain "add_vhost_test"
  end

  it "list all vhost with header if quiet?" do
    io = IO::Memory.new
    command = ListVhostCommand.new(io)
    ARGV.push("-s")
    command.run
    output = io.rewind.gets_to_end
    output.should_not contain "Listing vhosts ..."
    output.should contain "name"
    output.should contain "/"
    output.should contain "add_vhost_test"
  end
end

describe DeleteVhostCommand do
  it "delete vhost" do
    io = IO::Memory.new
    command = DeleteVhostCommand.new(io)
    ARGV.push("-p")
    ARGV.push("add_vhost_test")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Vhost deleted\n"
  end
end

describe SetVhostLimitCommand do
  it "set max-connections to a vhost" do
    io = IO::Memory.new
    command = SetVhostLimitCommand.new(io)
    Lavin::Httpclient::Vhost.create_or_update("vhost_limit_test")
    ARGV.push("-p")
    ARGV.push("vhost_limit_test")
    ARGV.push("{\"max-connections\": 10}")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Vhost vhost_limit_test limit max-connections set to 10\n"
  end

  it "set max_queues to a vhost" do
    io = IO::Memory.new
    command = SetVhostLimitCommand.new(io)
    Lavin::Httpclient::Vhost.create_or_update("vhost_limit_test")
    ARGV.push("-p")
    ARGV.push("vhost_limit_test")
    ARGV.push("{\"max-queues\": 5}")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Vhost vhost_limit_test limit max-queues set to 5\n"
  end

  it "set max-queues and max-connections to a vhost" do
    io = IO::Memory.new
    command = SetVhostLimitCommand.new(io)
    Lavin::Httpclient::Vhost.create_or_update("vhost_limit_test")
    ARGV.push("-p")
    ARGV.push("vhost_limit_test")
    ARGV.push("{\"max-queues\": 4,\"max-connections\": 8}")
    command.run
    output = io.rewind.gets_to_end
    output.should contain "Vhost vhost_limit_test limit max-connections set to 8\n"
    output.should contain "Vhost vhost_limit_test limit max-queues set to 4\n"
  end
end

describe SetPermissionsCommand do
  it "set permission for a user to a vhost" do
    io = IO::Memory.new
    vhost = "vhost_permission"
    user = "user_permission"
    command = SetPermissionsCommand.new(io)
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::User.create_or_update(name: user, password: "titi")
    configure = read = write = "all.*"
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("-u")
    ARGV.push(user)
    ARGV.push("-c")
    ARGV.push("#{configure}")
    ARGV.push("-r")
    ARGV.push("#{read}")
    ARGV.push("-w")
    ARGV.push("#{write}")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Permission to #{user} on #{vhost} updated\n"
  end
end

describe CreateQueue do
  it "create a queue" do
    io = IO::Memory.new
    vhost = "vhost_queue"
    queue_name = "queue_test"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    command = CreateQueue.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push(queue_name)
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Queue #{queue_name} created or update in vhost #{vhost}\n"
  end
end

describe ListQueue do
  it "list all queue and their properties" do
    io = IO::Memory.new
    vhost = "vhost_queue_list"
    queue_base_name = "queue_test_list"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Queue.create_or_update("#{queue_base_name}1", vhost)
    Lavin::Httpclient::Queue.create_or_update("#{queue_base_name}2", vhost)
    Lavin::Httpclient::Queue.create_or_update("#{queue_base_name}3", vhost)
    command = ListQueue.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    command.run
    output = io.rewind.gets_to_end
    output.should contain "Listing queues for vhost #{vhost} ...\n"
    output.should contain "name\tmessages\n"
    output.should contain "#{queue_base_name}1"
    output.should contain "#{queue_base_name}2"
    output.should contain "#{queue_base_name}3"
  end
end

describe PauseQueue do
  it "pause all consumers on a queue" do
    io = IO::Memory.new
    vhost = "vhost_queue_pause"
    queue_name = "queue_test_pause"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Queue.create_or_update(queue_name, vhost)
    command = PauseQueue.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push(queue_name)
    command.run
    output = io.rewind.gets_to_end
    output.should eq "All consummers of Queue #{queue_name} in vhost #{vhost} are paused\n"
  end
end

describe ResumeQueue do
  it "resume all consumers on a queue" do
    io = IO::Memory.new
    vhost = "vhost_queue_resume"
    queue_name = "queue_test_resume"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Queue.create_or_update(queue_name, vhost)
    Lavin::Httpclient::Queue.pause(queue_name, vhost)
    command = ResumeQueue.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push(queue_name)
    command.run
    output = io.rewind.gets_to_end
    output.should eq "All consummers of Queue #{queue_name} in vhost #{vhost} are resumed\n"
  end
end

describe PurgeQueue do
  it "pause all consumers on a queue" do
    io = IO::Memory.new
    vhost = "vhost_queue_purge"
    queue_name = "queue_test_purge"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Queue.create_or_update(queue_name, vhost)
    command = PurgeQueue.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push(queue_name)
    command.run
    output = io.rewind.gets_to_end
    output.should eq "All messages in Queue #{queue_name} in vhost #{vhost} are removed\n"
  end
end

describe DeleteQueue do
  it "Delete queue" do
    io = IO::Memory.new
    vhost = "vhost_queue_delete"
    queue_name = "queue_test_delete"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Queue.create_or_update(queue_name, vhost)
    command = DeleteQueue.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push(queue_name)
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Queue #{queue_name} in vhost #{vhost} is deleted\n"
  end
end

describe SetPolicy do
  it "set policies for a vhost" do
    io = IO::Memory.new
    vhost = "vhost_set_policy"
    policy_name = "test_set_policy"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    command = SetPolicy.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push(policy_name)
    ARGV.push("--pattern")
    ARGV.push("plop")
    ARGV.push("--definition")
    ARGV.push("{}")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Policy #{policy_name} in vhost #{vhost} is set\n"
  end
end

describe ListPolicies do
  it "return policies list with whost, name, pattern, apply-to, definition and priority" do
    io = IO::Memory.new
    vhost = "vhost_list_policy"
    policy_name = "list_policies"
    pattern = "pikachu"
    definition = Hash(String, String | Int32).new
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Policy.create_or_update("#{policy_name}1", vhost, pattern, definition)
    Lavin::Httpclient::Policy.create_or_update("#{policy_name}2", vhost, pattern, definition)
    Lavin::Httpclient::Policy.create_or_update("#{policy_name}3", vhost, pattern, definition)
    command = ListPolicies.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    command.run
    output = io.rewind.gets_to_end
    output.should contain "Listing policies for vhost #{vhost} ..."
    output.should contain "vhost\tname\tpattern\tapply-to\tdefinition\tpriority"
    output.should contain "#{vhost}\t#{policy_name}1\t#{pattern}\tall\t#{definition}\t0"
    output.should contain "#{vhost}\t#{policy_name}2\t#{pattern}\tall\t#{definition}\t0"
    output.should contain "#{vhost}\t#{policy_name}3\t#{pattern}\tall\t#{definition}\t0"
  end
end

describe ClearPolicy do
  it "Clears (removes) a policy" do
    io = IO::Memory.new
    vhost = "vhost_clear_policy"
    policy_name = "clear_policies"
    pattern = "pikachu"
    definition = Hash(String, String | Int32).new
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Policy.create_or_update("#{policy_name}1", vhost, pattern, definition)
    Lavin::Httpclient::Policy.create_or_update("#{policy_name}2", vhost, pattern, definition)
    Lavin::Httpclient::Policy.create_or_update("#{policy_name}3", vhost, pattern, definition)
    command = ClearPolicy.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push("#{policy_name}2")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Policy #{policy_name}2 removed in vhost #{vhost}\n"
    policies = Lavin::Httpclient::Policy.all(vhost)
    policies.size.should eq 2
  end
end

describe CreateExchange do
  it "create an exchange" do
    io = IO::Memory.new
    vhost = "vhost_create_exchange"
    exchange_name = "create_exchange"
    type = "direct"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    command = CreateExchange.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push(exchange_name)
    ARGV.push("--type")
    ARGV.push(type)
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Exchange #{exchange_name} created in vhost #{vhost}\n"
  end
end

describe ListExchange do
  it "lists exchanges" do
    io = IO::Memory.new
    vhost = "vhost_list_exchange"
    exchange_name = "create_exchange"
    type = "topic"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Exchange.create_or_update("#{exchange_name}1", vhost, type)
    Lavin::Httpclient::Exchange.create_or_update("#{exchange_name}2", vhost, type)
    Lavin::Httpclient::Exchange.create_or_update("#{exchange_name}3", vhost, type)
    command = ListExchange.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    command.run
    output = io.rewind.gets_to_end
    output.should contain "Listing exchanges for vhost #{vhost} ..."
    output.should contain "name\ttype"
    output.should contain "#{exchange_name}1\t#{type}"
    output.should contain "#{exchange_name}2\t#{type}"
    output.should contain "#{exchange_name}3\t#{type}"
  end
end

describe DeleteExchange do
  it "Delete exchange" do
    io = IO::Memory.new
    vhost = "vhost_delete_exchange"
    exchange_name = "delete_exchange"
    type = "fanout"
    Lavin::Httpclient::Vhost.create_or_update(vhost)
    Lavin::Httpclient::Exchange.create_or_update("#{exchange_name}1", vhost, type)
    Lavin::Httpclient::Exchange.create_or_update("#{exchange_name}2", vhost, type)
    Lavin::Httpclient::Exchange.create_or_update("#{exchange_name}3", vhost, type)
    command = DeleteExchange.new(io)
    ARGV.push("-p")
    ARGV.push(vhost)
    ARGV.push("--name")
    ARGV.push("#{exchange_name}3")
    command.run
    output = io.rewind.gets_to_end
    output.should eq "Exchange #{exchange_name}3 deleted\n"
    exchanges = Lavin::Httpclient::Exchange.all(vhost)
    exchanges.size.should eq 8
  end
end
