require "option_parser"
require "lavin-httpclient"

abstract class Command
  abstract def run
  abstract def slug
  abstract def description
  abstract def args
  abstract def specific_options
  abstract def validate?
  abstract def process_command

  @parser = OptionParser.new
  @options = {} of String => String

  def initialize(@io : IO = STDOUT)
    specific_options
    global_options
  end

  def run
    @parser.parse
    if validate?
      process_command
    else
      @io.puts @parser
    end
  end

  def banner
    "Usage : #{PROGRAM_NAME} #{slug} [OPTIONS] #{args}"
  end

  def global_options
    @parser.banner = banner
    @parser.on("-p vhost", "--vhost=vhost", "Specify vhost") do |v|
      @options["vhost"] = v
    end
    @parser.on("-H host", "--host=host", "Specify host") do |v|
      ENV["LAVINMQCTL_HOST"] = v
      @options["host"] = v
    end
    @parser.on("-n node", "--node=node", "Specify node") do |v|
      @options["node"] = v
    end
    @parser.on("-q", "--quiet", "suppress informational messages") do
      @options["quiet"] = "yes"
    end
    @parser.on("-s", "--silent", "suppress informational messages and table formatting") do
      @options["silent"] = "yes"
    end
    @parser.on("-h", "--help", "Show this help") do
    end
    @parser.invalid_option do
    end
    @parser.missing_option do
    end
  end

  def quiet?
    @options["quiet"]? || @options["silent"]?
  end
end

class ListExchange < Command
  getter slug : String = "list_exchanges"
  getter description : String = "lists exchanges"
  getter args : String = ""

  def validate?
    true
  end

  def specific_options
    ""
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    @io.puts "Listing exchanges for vhost #{vhost} ..." unless quiet?
    @io.puts "name\ttype"
    exchanges = Lavin::Httpclient::Exchange.all(vhost)
    exchanges.each do |exchange|
      @io.puts "#{exchange.name}\t#{exchange.type}"
    end
  end
end

class DeleteExchange < Command
  getter slug : String = "delete_exchange"
  getter description : String = "Delete exchange"
  getter args : String = ""

  def validate?
    @options["exchange_name"]?
  end

  def specific_options
    @parser.on("--name=exchange_name", "Specify name of the queue") do |v|
      @options["exchange_name"] = v
    end
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    exchange_name = @options["exchange_name"]
    Lavin::Httpclient::Exchange.delete(exchange_name, vhost)
    @io.puts "Exchange #{exchange_name} deleted"
  end
end

class CreateExchange < Command
  getter slug : String = "create_exchange"
  getter description : String = "Create an exchange in vhost"
  getter args : String = ""
  getter arguments : Hash(String, String | UInt32) = Hash(String, String | UInt32).new

  def validate?
    @options["exchange_name"]? && @options["type"]?
  end

  def specific_options
    @parser.on("--name=exchange_name", "Specify name of the queue") do |v|
      @options["exchange_name"] = v
    end
    @parser.on("--type=type", "direct,fanout,topic,headers,x-delayed-message,x-federation-upstream,x-consistent-hash") do |v|
      @options["type"] = v
    end
    @parser.on("--auto-delete", "Auto delete exchange when they are no longer in use") do
      @options["auto_delete"] = "true"
    end
    @parser.on("--durable", "Make the exchange durable") do
      @options["durable"] = "true"
    end
    @parser.on("--internal", "for internal use") do
      @options["internal"] = "true"
    end
    @parser.on("--delayed", "introduce a delay before delivering messages") do
      @options["delayed"] = "true"
    end
    @parser.on("--x-alternate-exchange=value", "If messages to this exchange cannot otherwise be routed, send them to the alternate exchange named here.") do |v|
      @arguments["x-alternate-exchange"] = v
    end
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    exchange_name = @options["exchange_name"]
    Lavin::Httpclient::Exchange.create_or_update(
      exchange_name,
      vhost,
      @options["type"],
      @options.has_key?("auto_delete"),
      @options.has_key?("durable"),
      @options.has_key?("internal"),
      @options.has_key?("delayed"),
      @arguments)
    @io.puts "Exchange #{exchange_name} created in vhost #{vhost}"
  end
end

class ListQueue < Command
  getter slug : String = "list_queue"
  getter description : String = "Lists queues and their properties"
  getter args : String = ""

  def validate?
    true
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    queues = Lavin::Httpclient::Queue.all(vhost)
    @io.puts "Listing queues for vhost #{vhost} ..." unless quiet?
    @io.puts "name\tmessages"
    queues.each do |queue|
      @io.puts "#{queue.name}\t#{queue.messages}"
    end
  end

  def specific_options
  end
end

class PauseQueue < Command
  getter slug : String = "pause_queue"
  getter description : String = "Pause all consumers on a queue"
  getter args : String = ""

  def validate?
    @options["queue_name"]?
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    queue_name = @options["queue_name"]
    Lavin::Httpclient::Queue.pause(queue_name, vhost)
    @io.puts "All consummers of Queue #{queue_name} in vhost #{vhost} are paused"
  end

  def specific_options
    @parser.on("--name=queue_name", "Specify name of the queue") do |v|
      @options["queue_name"] = v
    end
  end
end

class ResumeQueue < Command
  getter slug : String = "resume_queue"
  getter description : String = "Resume all consumers on a queue"
  getter args : String = ""

  def validate?
    @options["queue_name"]?
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    queue_name = @options["queue_name"]
    Lavin::Httpclient::Queue.resume(queue_name, vhost)
    @io.puts "All consummers of Queue #{queue_name} in vhost #{vhost} are resumed"
  end

  def specific_options
    @parser.on("--name=queue_name", "Specify name of the queue") do |v|
      @options["queue_name"] = v
    end
  end
end

class DeleteQueue < Command
  getter slug : String = "delete_queue"
  getter description : String = "Delete queue"
  getter args : String = ""

  def validate?
    @options["queue_name"]?
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    queue_name = @options["queue_name"]
    Lavin::Httpclient::Queue.delete(queue_name, vhost)
    @io.puts "Queue #{queue_name} in vhost #{vhost} is deleted"
  end

  def specific_options
    @parser.on("--name=queue_name", "Specify name of the queue") do |v|
      @options["queue_name"] = v
    end
  end
end

class PurgeQueue < Command
  getter slug : String = "purge_queue"
  getter description : String = "Purges a queue (removes all messages in it)"
  getter args : String = ""

  def validate?
    @options["queue_name"]?
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    queue_name = @options["queue_name"]
    Lavin::Httpclient::Queue.purge(queue_name, vhost)
    @io.puts "All messages in Queue #{queue_name} in vhost #{vhost} are removed"
  end

  def specific_options
    @parser.on("--name=queue_name", "Specify name of the queue") do |v|
      @options["queue_name"] = v
    end
  end
end

class ListPolicies < Command
  getter slug : String = "list_policies"
  getter description : String = "Lists all policies in a virtual host"
  getter args : String = ""

  def validate?
    true
  end

  def specific_options
    ""
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    @io.puts "Listing policies for vhost #{vhost} ..." unless quiet?
    @io.puts "vhost\tname\tpattern\tapply-to\tdefinition\tpriority"
    policies = Lavin::Httpclient::Policy.all
    policies.each do |policy|
      @io.puts "#{policy.vhost}\t#{policy.name}\t#{policy.pattern}\t#{policy.applyto}\t#{policy.definition}\t#{policy.priority}"
    end
  end
end

class ClearPolicy < Command
  getter slug : String = "clear_policy"
  getter description : String = "Clears (removes) a policy"
  getter args : String = ""

  def validate?
    @options["policy_name"]?
  end

  def specific_options
    @parser.on("--name=policy_name", "Specify name of the policy") do |v|
      @options["policy_name"] = v
    end
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    Lavin::Httpclient::Policy.delete(@options["policy_name"], vhost)
    @io.puts "Policy #{@options["policy_name"]} removed in vhost #{vhost}"
  end
end

class SetPolicy < Command
  getter slug : String = "set_policy"
  getter description : String = "Set policy to a vhost"
  getter args : String = ""
  getter definitions : Hash(String, String | Int32) = Hash(String, String | Int32).new

  def validate?
    @options["policy_name"]? && @options["pattern"]?
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    policy_name = @options["policy_name"]
    pattern = @options["pattern"]
    priority = @options["priority"]?.try &.to_i? || 0
    applyto = @options["apply-to"]? || "all"
    Lavin::Httpclient::Policy.create_or_update(
      policy_name,
      vhost,
      pattern,
      @definitions,
      priority,
      applyto)
    @io.puts "Policy #{policy_name} in vhost #{vhost} is set"
  end

  def specific_options
    @parser.on("--name=policy_name", "Specify name of the policy") do |v|
      @options["policy_name"] = v
    end
    @parser.on("--pattern=pattern", "Specify a pattern") do |v|
      @options["pattern"] = v
    end
    @parser.on("--priority=priority", "Specify priority") do |v|
      @options["priority"] = v
    end
    @parser.on("--apply-to=apply-to", "Apply-to") do |v|
      @options["apply-to"] = v
    end
    @parser.on("--max-length", "Set a max length for the queue") do |v|
      @definitions["max-length"] = v.to_i32
    end
    @parser.on("--max-length-bytes", "Set a max length in bytes for the queue") do |v|
      @definitions["max-length-bytes"] = v.to_i32
    end
    @parser.on("--message-ttl", "Message time to live") do |v|
      @definitions["message-ttl"] = v.to_i32
    end
    @parser.on("--overflow", "this determines what happens to messages when the maximum length of queue is reached") do |v|
      @definitions["overflow"] = v
    end
    @parser.on("--auto-expires", "How long a queue can be unused before it is automatically deleted") do |v|
      @definitions["expires"] = v.to_i32
    end
    @parser.on("--dead-letter-exchange", "To which exchange to dead letter messages") do |v|
      @definitions["dead-letter-exchange"] = v
    end
    @parser.on("--dead-letter-routing-key", "Which routing key to use when dead lettering") do |v|
      @definitions["dead-letter-routing-key"] = v
    end
    @parser.on("--federation-upstream", "Chooses a specific upstream set to use for federation") do |v|
      @definitions["federation-upstream"] = v
    end
    @parser.on("--federation-upstream-set", "Set the name of upstream") do |v|
      @definitions["federation-upstream-set"] = v
    end
    @parser.on("--delivery-limit", "How many time a message will be delivered before dead lettered") do |v|
      @definitions["delivery-limit"] = v.to_i32
    end
    @parser.on("--max-age", "Stream queue segments will be delete when all messages in the segmented is older than this") do |v|
      @definitions["max-age"] = v
    end
  end
end

class CreateQueue < Command
  getter slug : String = "create_queue"
  getter description : String = "Create queue"
  getter args : String = "<args>"
  getter arguments : Hash(String, String | UInt32) = Hash(String, String | UInt32).new

  def validate?
    true
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    queue_name = @options["queue_name"]
    durable = @options["durable"]?
    auto_delete = @options["auto_delete"]?
    Lavin::Httpclient::Queue.create_or_update(
      queue_name,
      vhost,
      durable,
      auto_delete,
      @arguments)
    @io.puts "Queue #{queue_name} created or update in vhost #{vhost}"
  end

  def specific_options
    @parser.on("-x queue_name", "--name=queue_name", "Specify name of the queue") do |v|
      @options["queue_name"] = v
    end
    @parser.on("--auto-delete", "Auto delete queue when last consumer is removed") do
      @options["auto_delete"] = "true"
    end
    @parser.on("--durable", "Make the queue durable") do
      @options["durable"] = "true"
    end
    @parser.on("--expires", "") do |v|
      @arguments["x-expires"] = v.to_u32
    end
    @parser.on("--max-length", "Set a max length for the queue") do |v|
      @arguments["x-max-length"] = v.to_u32
    end
    @parser.on("--message-ttl", "Message time to live") do |v|
      @arguments["x-message-ttl"] = v.to_u32
    end
    @parser.on("--delivery-limit", "How many time a message will be delivered before dead lettered") do |v|
      @arguments["x-delivery-limit"] = v.to_u32
    end
    @parser.on("--reject-on-overflow", "Reject publish if max-length is met, otherwise messages in the queue is dropped") do
      @arguments["x-overflow"] = "reject-publish"
    end
    @parser.on("--dead-letter-exchange", "To which exchange to dead letter messages") do |v|
      @arguments["x-dead-letter-exchange"] = v
    end
    @parser.on("--dead-letter-routing-key", "Which routing key to use when dead lettering") do |v|
      @arguments["x-dead-letter-routing-key"] = v
    end
    @parser.on("--stream-queue", "Create a Stream Queue") do
      @arguments["x-queue-type"] = "stream"
    end
  end
end

class SetPermissionsCommand < Command
  getter slug : String = "set_permissions"
  getter description : String = "Set permissions for a user"
  getter args : String = ""

  def validate?
    @options["username"]? && @options["configure"]? && @options["read"]? && @options["write"]?
  end

  def process_command
    vhost = @options["vhost"]? || "/"
    user = @options["username"]
    Lavin::Httpclient::Permission.create_or_update(
      user,
      vhost,
      @options["configure"],
      @options["read"],
      @options["write"])
    @io.puts "Permission to #{user} on #{vhost} updated"
  end

  def specific_options
    @parser.on("-u username", "--username=username", "Specify username") do |v|
      @options["username"] = v
    end
    @parser.on("-c configure", "--configure=\".*\"", "Specify configure permission") do |v|
      @options["configure"] = v
    end
    @parser.on("-r read", "--read=\".*\"", "Specify read permission") do |v|
      @options["read"] = v
    end
    @parser.on("-w write", "--write=\".*\"", "Specify write permission") do |v|
      @options["write"] = v
    end
  end
end

class SetVhostLimitCommand < Command
  getter slug : String = "set_vhost_limits"
  getter description : String = "Set VHost limits (max-connections, max-queues)"
  getter args : String = "<json>"
  getter specific_options : String = ""

  def validate?
    @options["vhost"]?
  end

  def process_command
    data = ARGV.shift?
    if data.nil?
      @io.puts @parser
    else
      parse_data(data)
    end
  end

  private def parse_data(data : String)
    json = JSON.parse(data)
    ok = false
    if max_connections = json["max-connections"]?.try(&.as_i?)
      set_max_connections(max_connections)
      ok = true
    end
    if max_queues = json["max-queues"]?.try(&.as_i?)
      set_max_queues(max_queues)
      ok = true
    end
    @io.puts "Invalid data" unless ok
  end

  private def set_max_connections(max_connections : Int32)
    vhost = @options["vhost"]
    Lavin::Httpclient::VhostLimit.set_max_connections(vhost, max_connections)
    @io.puts "Vhost #{vhost} limit max-connections set to #{max_connections}"
  end

  private def set_max_queues(max_queues : Int32)
    vhost = @options["vhost"]
    Lavin::Httpclient::VhostLimit.set_max_queues(vhost, max_queues)
    @io.puts "Vhost #{vhost} limit max-queues set to #{max_queues}"
  end
end

class AddVhostCommand < Command
  getter slug : String = "add_vhost"
  getter description : String = "Creates a virtual host"
  getter args : String = ""
  getter specific_options : String = ""

  def validate?
    @options["vhost"]?
  end

  def process_command
    Lavin::Httpclient::Vhost.create_or_update(@options["vhost"])
    @io.puts "Vhost added"
  end
end

class DeleteVhostCommand < Command
  getter slug : String = "delete_vhost"
  getter description : String = "Deletes a virtual host"
  getter args : String = ""
  getter specific_options : String = ""

  def validate?
    @options["vhost"]?
  end

  def process_command
    Lavin::Httpclient::Vhost.delete(@options["vhost"])
    @io.puts "Vhost deleted"
  end
end

class ListVhostCommand < Command
  getter slug : String = "list_vhost"
  getter description : String = "Lists virtual hosts"
  getter args : String = ""
  getter specific_options : String = ""

  def validate?
    true
  end

  def process_command
    vhosts = Lavin::Httpclient::Vhost.all
    @io.puts "Listing vhosts ..." unless quiet?
    if vhosts.size > 0
      @io.puts "name"
      vhosts.each do |vhost|
        @io.puts vhost.name
      end
    else
      @io.puts "invalid data"
    end
  end
end

class StatusCommand < Command
  getter slug : String = "status"
  getter description : String = "Show server informations"
  getter args : String = ""
  getter specific_options : String = ""

  def validate?
    true
  end

  def process_command
    informations = Lavin::Httpclient::Status.get
    @io.puts "Version: #{informations.lavinmq_version}"
    @io.puts "Node: #{informations.node}"
    @io.puts "Uptime: #{informations.uptime}"
    @io.puts "Connections: #{informations.object_totals.connections}"
    @io.puts "Channels: #{informations.object_totals.channels}"
    @io.puts "Consumers: #{informations.object_totals.consumers}"
    @io.puts "Exchanges: #{informations.object_totals.exchanges}"
    @io.puts "Queues: #{informations.object_totals.queues}"
    @io.puts "Messages: #{informations.queue_totals.messages}"
    @io.puts "Messages ready: #{informations.queue_totals.messages_ready}"
    @io.puts "Messages unacked: #{informations.queue_totals.messages_unacknowledged}"
  end
end

class SetUserTagsCommand < Command
  getter slug : String = "set_user_tags"
  getter description : String = "Sets user tags"
  getter args : String = ""

  def validate?
    @options["username"]? && @options["tags"]?
  end

  def process_command
    tags = parse_tags
    case tags
    when Array
      Lavin::Httpclient::User.create_or_update(name: @options["username"], tags: tags)
      @io.puts "User tags Modified"
    else
      @io.puts tags
    end
  end

  def parse_tags
    tags = Array(Lavin::Httpclient::Tags).new
    @options["tags"].split(",").each do |tag|
      tags << Lavin::Httpclient::Tags.parse(tag)
    end
    tags
  rescue
    "Wrong tags"
  end

  def specific_options
    @parser.on("-u username", "--username=username", "Specify username") do |v|
      @options["username"] = v
    end
    @parser.on("-t tags", "--tags=tag1,tag2", "Specify tags") do |v|
      @options["tags"] = v
    end
  end
end

class ListUserCommand < Command
  getter slug : String = "list_users"
  getter description : String = "List user names and tags"
  getter args : String = ""

  def validate?
    true
  end

  def process_command
    users = Lavin::Httpclient::User.all
    @io.puts "Listing users ..." unless quiet?
    columns = %w[name tags]
    @io.puts columns.join("\t")
    users.each do |user|
      @io.puts [user.name, user.tags].join("\t")
    end
  end

  def specific_options
  end
end

class DeleteUserCommand < Command
  getter slug : String = "delete_user"
  getter description : String = "Delete a user"
  getter args : String = ""

  def validate?
    @options["username"]?
  end

  def process_command
    Lavin::Httpclient::User.delete(@options["username"])
    @io.puts "User deleted"
  end

  def specific_options
    @parser.on("-u username", "--username=username", "Specify username") do |v|
      @options["username"] = v
    end
  end
end

class AddUserCommand < Command
  getter slug : String = "add_user"
  getter description : String = "Creates a new user"
  getter args : String = ""

  def validate?
    @options["username"]? && @options["password"]?
  end

  def process_command
    Lavin::Httpclient::User.create_or_update(@options["username"], @options["password"])
    @io.puts "User Created"
  end

  def specific_options
    @parser.on("-u username", "--username=username", "Specify username") do |v|
      @options["username"] = v
    end
    @parser.on("-w password", "--password=password", "Specify password") do |v|
      @options["password"] = v
    end
  end
end

class ChangePasswordCommand < AddUserCommand
  getter slug : String = "change_password"
  getter description : String = "Change the user password"

  def process_command
    Lavin::Httpclient::User.create_or_update(@options["username"], @options["password"])
    @io.puts "User Modified"
  end
end

class Cli
  @commands : Array(Command)
  @io : IO

  def initialize(@commands, @io = STDOUT)
  end

  def run
    option_parser
  end

  def option_parser
    cli_parser = OptionParser.new do |parser|
      parser.banner = "Usage: #{PROGRAM_NAME} <command> <args>\nAvailables command"
      @commands.each do |command|
        parser.on(command.slug, command.description) do
          command.run
        end
      end
      parser.on("-h", "--help", "Show this help") do
        @io.puts parser
      end
      parser.invalid_option do
        @io.puts parser
      end
      parser.missing_option do
        @io.puts parser
      end
    end
    cli_parser.parse
  end
end
