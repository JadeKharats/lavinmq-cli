require "./lavinmq-cli"

Cli.new([
  AddUserCommand.new,
  ChangePasswordCommand.new,
  ListUserCommand.new,
  DeleteUserCommand.new,
  SetUserTagsCommand.new,
  StatusCommand.new,
  ListQueue.new,
  PauseQueue.new,
  ResumeQueue.new,
  DeleteQueue.new,
  PurgeQueue.new,
  CreateQueue.new,
  ListPolicies.new,
  ClearPolicy.new,
  SetPolicy.new,
  SetPermissionsCommand.new,
  SetVhostLimitCommand.new,
  AddVhostCommand.new,
  DeleteVhostCommand.new,
  ListVhostCommand.new,
]).run
